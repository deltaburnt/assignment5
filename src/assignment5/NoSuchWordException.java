package assignment5;

public class NoSuchWordException extends Exception
{
    private static final long serialVersionUID = 1L;

    public NoSuchWordException(String message)
    {
        super(message);
    }

    public NoSuchWordException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}
