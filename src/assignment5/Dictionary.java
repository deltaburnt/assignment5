package assignment5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * 
 * Dictionary
 * Holds all 5 letter dictionary words for word ladders
 *
 */

public class Dictionary
{
	private HashSet<String> dictionary = new HashSet<String>();
	
	Dictionary(String filename)
	{
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			String nextWord = reader.readLine();
			
			while (nextWord != null)
			{
				if (nextWord.charAt(0) != '*')
				{
					dictionary.add(nextWord.substring(0, 5));
				}
				
				nextWord = reader.readLine();
			}
		} catch (IOException e)
		{
			System.out.println("IOException Error: Could not read in dictionary.");
		}
	}
	
	/**
	 * Finds all words one letter away from source, favoring words closer to target.
	 * @param source the starting word for the search
	 * @param target the end goal word
	 * @param pivot the letter changed before getting arriving at source
	 * @return list of all words one letter away from source
	 */
	public List<String> getSimilarWords(String source, String target, int pivot)
	{
		List<String> foundWords = new ArrayList<String>();

		for (String word : dictionary)
		{
			if (pivot != -1 && word.charAt(pivot) != source.charAt(pivot)) { continue; }

			if (stringDiff(word, source) != 1) { continue; }
			
			foundWords.add(stringDiff(word, target) + word); // Insert word prefixed by it's diff from target
		}
		
		foundWords.sort(null);
		
		return foundWords;
	}
	
	/**
	 * Determines if word is in the dictionary
	 * @param word
	 * @return true if word is in dictionary, false otherwise
	 */
	public boolean contains(String word)
	{
		return dictionary.contains(word);
	}
	
	/**
	 * Finds the difference between two strings of same length
	 * @param s1 input string 1
	 * @param s2 input string 2
	 * @return the number of characters between s1 and s2 that are not the same
	 */
	public static int stringDiff(String s1, String s2)
	{
		int diff = 0;

		for (int i = 0; i < s1.length(); i++)
		{
			if (s1.charAt(i) != s2.charAt(i)) { diff++; }
		}
		
		return diff;
	}
}
