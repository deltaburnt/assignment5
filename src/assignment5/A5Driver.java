/* EE422C Assignment 5 */
/* Student Name: Cassidy Burden (eid: cab5534), Nishant Raman, Joshua Gnanayutham, Lab Section: Xavier (Th 9:30-11:00)*/


package assignment5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class A5Driver
{
	public static void main(String[] args) throws NoSuchLadderException
	{
		// Create a word ladder solver object
		Dictionary dictionary = new Dictionary("assn5words.dat");
        Assignment5Interface wordLadderSolver = new WordLadderSolver(dictionary);
        StopWatch watch = new StopWatch();

        watch.start();
        
        // Read file in
        // for each line in the file call compute ladder
        ArrayList<String> inputWords = new ArrayList<String>();
        try
        {
        	BufferedReader reader = new BufferedReader(new FileReader("assn5data.txt"));
        	String words = reader.readLine();
        	while (words != null)
        	{
        		inputWords.add(words);
        		words = reader.readLine();
        	}
        }
        catch (IOException e)
        {
        	e.printStackTrace();
        }
        
        for(String words : inputWords)
    	{
	        try
	        {
        		String[] wordPair = words.split(" +");
        		
        		if (!dictionary.contains(wordPair[0]) || !dictionary.contains(wordPair[1]))
        		{
        			throw new NoSuchWordException("At least one of the words "
        			                              + wordPair[0]
        			                              + " and "
        			                              + wordPair[1] + " are not legitimate "
        			                              + "5-letter words from the dictionary");
        		}
	            List<String> result = wordLadderSolver.computeLadder(wordPair[0], wordPair[1]);
	            //boolean correct = wordLadderSolver.validateResult(wordPair[0], wordPair[1], result);
	            //System.out.println(correct);
	            for (String s : result)
	            {
	            	System.out.println(s);
	            }
	            
	            System.out.println("**********");
        	}
	        catch (NoSuchLadderException e) 
	        {
	            System.out.println(e.getMessage());
	            System.out.println("**********");
	        }
	        catch (NoSuchWordException e)
	        {
	        	System.out.println(e.getMessage());
	        	System.out.println("**********");
	        }
    	}
        
        System.out.println("Execution time: " + watch.getElapsedTime() / watch.NANOS_PER_SEC + " seconds");
	}

}
