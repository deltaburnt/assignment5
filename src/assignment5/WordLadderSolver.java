package assignment5;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class WordLadderSolver implements Assignment5Interface
{
	private Dictionary dictionary;
	private ArrayList<String> solutionList = new ArrayList<String>();
	private HashSet<String> seenWords = new HashSet<String>();

	WordLadderSolver(Dictionary dictionary)
	{
		this.dictionary = dictionary;
	}
	
	public List<String> computeLadder(String source, String target) throws NoSuchLadderException
	{
		solutionList.clear();
		seenWords.clear();

		if (makeLadder(source, target, -1))
		{
			return (List<String>) solutionList.clone();
		}
		
		throw new NoSuchLadderException("No word ladder found from " + source + " to " + target);
	}
	
	/**
	 * Finds word ladder between source and target, adding to solutionList the correct wordLadder
	 * @param source starting word
	 * @param target word to end on
	 * @param pivot letter last changed before arriving at source
	 * @return true if word ladder exists, false otherwise
	 */
	private boolean makeLadder(String source, String target, int pivot)
	{
		List<String> foundWords = dictionary.getSimilarWords(source, target, pivot);
		solutionList.add(source);
		seenWords.add(source);
		
		if (source.equals(target)) { return true; }
		
		for (String word : foundWords)
		{
			word = word.substring(1);
			if (seenWords.contains(word)) { continue; } // Memoized previous recursive trees
			
			int newPivot = findPivot(word, source);
			if(makeLadder(word, target, newPivot)) { return true; }
		}

		solutionList.remove(solutionList.size() - 1); // Pop off current source because not in solution path
		return false;
	}
	
	private int findPivot(String s1, String s2)
	{
		for (int i = 0; i < s1.length(); i++)
		{
			if (s1.charAt(i) != s2.charAt(i)) { return i; }
		}
		
		return -1;
	}

	@Override
	public boolean validateResult(String startWord, String endWord, List<String> wordLadder)
	{
		if (!endWord.equals(wordLadder.get(wordLadder.size() - 1)) || !startWord.equals(wordLadder.get(0)))
		{
			return false;
		}
		
		int previousPivot = -1;
		for (int i = 0; i < wordLadder.size() - 1; i++)
		{
			if (Dictionary.stringDiff(wordLadder.get(i), wordLadder.get(i + 1)) != 1)
			{
				return false;
			}
			
			if (!dictionary.contains(wordLadder.get(i)))
			{
				return false;
			}
			
			int newPivot = findPivot(wordLadder.get(i), wordLadder.get(i + 1));
			if (newPivot == previousPivot)
			{
				return false;
			}
			
			previousPivot = newPivot;
		}
		
		return true;
	}
}