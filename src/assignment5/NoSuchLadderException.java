package assignment5;

/**
 * 
 * NoSuchLadderException
 * Thrown when valid word ladder cannot be found
 *
 */

public class NoSuchLadderException extends Exception
{
    private static final long serialVersionUID = 1L;

    public NoSuchLadderException(String message)
    {
        super(message);
    }

    public NoSuchLadderException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}
